#include <stdio.h>
#include <math.h>
float input()
{
	float x;
	printf("Enter the coordinates: \n");
	scanf("%f",&x);
	return x;
}
float compute(float x1,float y1,float x2,float y2)
{
	float d;
	d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
	return d;
}
void output(float p)
{
	printf("The Distance between the coordinates is: %.2f",p);
}
int main()
{
	float a,b,c,d,p;
	a=input();
	b=input();
	c=input();
	d=input();
	p=compute(a,b,c,d);
	output(p);
	return 0;
}