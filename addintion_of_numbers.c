#include <stdio.h>
#include <math.h>
int input()
{
	int a;
	printf("enter the number: \n");
	scanf("%d",&a);
	return a;
}
int compute(int a)
{
	int x=0;
	while(a!=0)
	{
		int rem=a%10;
		x=x+rem;
		a=a/10;
	}
	return x;
}
void output(int x)
{
	printf("The the sum of all the numbers is= %d\n",x);
}
int main()
{
	int a,x;
	a=input();
	x=compute(a);
	output(x);
	return 0;
}