#include<stdio.h>
#include<math.h>
int main()
{ 
   float x1,y1,x2,y2,d;
   printf("Enter the coordinates: \n");
   scanf("%f%f%f%f",&x1,&y1,&x2,&y2);
   d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
   printf("So the distance between the coordinates ( %.1f ,%.1f) and ( %.1f ,%.1f ) is %.3f.\n",x1,y1,x2,y2,d);
   return 0;
}