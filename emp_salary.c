#include <stdio.h>
struct st
{
	char name[15];
	int age;
	float salary;
};
typedef struct st S;
S input()
{
		S e;
        printf("Enter name : ");
		gets(e.name);
        printf("\n");
        printf("Enter age and salary  (lacs)\n");
		scanf("%d",&e.age);
        scanf("%f",&e.salary);
        printf("\n");
		return e;
}
void output(S e)
{
	printf("Name : %s.\n%d years. \n Rs.%.2f\n",e.name,e.age,e.salary);
}
int main()
{
	S e;
	e=input();
	output(e);
	return 0;
}