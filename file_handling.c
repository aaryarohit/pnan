#include <stdio.h>
int main()
{
    FILE *fp;
    char x;
    printf("enter data to the file\n");
    fp=fopen("INPUT.dat","w");
    while((x=getchar())!= EOF)
        fputc(x,fp);
    fclose(fp);
    printf("Contents of the file\n");
    fp=fopen("INPUT.dat","r");
    while((x=fgetc(fp))!= EOF)
        printf("%c",x);
    fclose(fp);
}
