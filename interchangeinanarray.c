 #include<stdio.h>
int input()
{
    int n;
    printf("Enter the number:");
    scanf("%d",&n);
    return n;
}
int arr_ip(int n,int arr[])
{
    for(int i=0;i<n;i++)
    {
        arr[i]=input();
    }
}
int interchange(int n,int arr[])
{
    int sp=0,lp=0,s=0,l=0;
    for(int i=0;i<n;i++)
    {
        if(arr[i]<=s)
        {
            s=arr[i];
            sp=i;
        }
        if(l<=arr[i])
        {
            l=arr[i];
            lp=i;
        }
    }
    printf("Largest Number is %d and is at position: %d\n",l,lp+1);
    printf("Smallest Number is %d and is at position: %d\n",s,sp+1);
    int temp=arr[lp];
    arr[lp]=arr[sp];
    arr[sp]=temp;
}
void output(int n,int arr[])
{
    for(int i=0;i<n;i++)
    {
        printf(" %d ",arr[i]);
        printf("\n");
    }
}
int main()
{
    int n;
    printf("Enter Size of Array (n+1)");
    n=input();
    int arr[n];
    printf("\n");
    printf("Enter Array Elements... \n");
    arr_ip(n,arr);
    printf("Original Array Elements are: \n");
    output(n,arr);
    interchange(n,arr);
    printf("Modified Array Elements are: \n");
    output(n,arr);
    return 0;
}