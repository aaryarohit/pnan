#include<stdio.h>
int ip()
{
    int x;
    printf("Enter a number\n");
    scanf("%d\n",&x);
    return x;
}
void swap(int *x,int *y)
{
    *x=*x+*y;
    *y=*x-*y;
    *x=*x-*y;
}
void output(int *x,int *y)
{
    printf("%d & %d\n",*x,*y);
}
int main()
{
    int x,y;
    x=ip();
    y=ip();
    output(&x,&y);
    swap(&x,&y);
    output(&x,&y);
    return 0;
}