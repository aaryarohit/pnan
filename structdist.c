#include<stdio.h>
#include<math.h>
struct pt
{
	float x;
	float y;
};
typedef struct pt P;
P input()
{
	P p;
	printf("Enter the coordinates: ");
	scanf("%f%f",&p.x,&p.y);
	return p;
}
float compute(P a,P b)
{
	float q;
	q=sqrt(pow((b.x-a.x),2)+pow((b.y-a.y),2));
	return q;
}
void output(float d)
{
	printf("The Distance between the coordinates is  %f",d);
}
int main()
{
	P a,b;
	float d;
	a=input();
	b=input();
	d=compute(a,b);
	output(d);
	return 0;
}