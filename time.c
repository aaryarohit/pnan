#include <stdio.h>
#include <math.h>
struct pt
{
	int x;
	int y;
};
typedef struct pt P;
P input()
{
	P p;
	printf("Enter the time in hh mm format: \d");
	scanf("%d%d",&p.x,&p.y);
	return p;
}
int compute(P p)
{
	int n;
	n=p.x*60+p.y;
	return n;
}
void output(int n)
{
	printf("The total time in minutes  is: %d \n",n);
}
int main()
{
	P h;
	int n;
	h=input();
	n=compute(h);
	output(n);
	return 0;
}